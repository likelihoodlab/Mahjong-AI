#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <set>
#include "pugixml-1.9/src/pugixml.hpp"
#include "pugixml-1.9/src/pugixml.cpp"
#include "riichi.hpp"
using namespace std;
using namespace pugi;

const int player_num = 4; // 玩家数
const int hai_len = 136; // 手牌数组长度

class confrontation // 对局信息
{
private: // 私有成员变量
    unsigned char hai[player_num][hai_len]; // 手牌
    unsigned char discard[player_num][hai_len]; // 弃牌
    unsigned char reach[player_num]; // 立直情况。0为未立直，1为立直。
    unsigned char show_num[player_num]; // 各家副露数
    unsigned char discard_num[player_num]; // 记录各家弃牌数量，用于区分早巡、中巡、晚巡。
    unsigned char discard_reach[player_num]; // 各家立直宣言牌
    unsigned char recently_reach[player_num]; // 最近一巡且没有鸣牌时四家的立直情况（用于判断一发）。值为2是立直的step1，step1之后的弃牌会将该值改为1，而后如果转一圈回到自己或者有副露，该值变为0。

    set<unsigned char> show[player_num]; // 记录各家鸣牌
    set<unsigned char> dora; // 记录宝牌指示牌
    set<unsigned char> discard_state[player_num][3]; // 不同阶段的弃牌。discard_state[][0]是早巡，[][1]是中巡，[][2]是晚巡
    set<unsigned char> discard_reach_state[player_num][2]; // 立直前后的弃牌。discard_reach_state[][0]是立直前，[][1]是立直后

    vector<unsigned char> discard_from_hand[player_num]; // 最近的手切
    vector<size_t> vec_index[4]; // 行偏移记录向量

    int field; // 场次
    int point[4]; // 打点
    int my_rank; // 自己的排名。0~3
    int perspective; // 视角，用0~3表示

    unsigned char showed_num; // 记录当前可能被副露的弃牌，用于输出吃碰操作的牌

    struct _state // 可以操作的类型
    {
        unsigned char can_discard; // 弃牌
        unsigned char can_chi;  // 吃
        unsigned char can_pon; // 碰
        unsigned char can_riichi; // 立直
        unsigned char can_kan; // 杠
        unsigned char pad1, pad2, pad3; // 填充位
    } __attribute__ ((aligned (8)));
    _state state[4];
    unsigned long long (&state_ptr)[4] = reinterpret_cast<unsigned long long (&)[4]>(state);

    enum next_op_type {next_op_discard, next_op_chi, next_op_pon, next_op_reach, next_op_else};
    struct _next_op
    {
        next_op_type op; // 下个操作的类型
        int who; // 下个操作的人
        int hai; // 下个操作的牌
    } next_op;

    ostringstream out_ss[4]; // out[0]是弃牌，out[1]是吃，out[2]是碰，out[3]是立直
    
    const char int_to_char[10] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

private: // 私有成员函数
    void update_point(const char_t *ten);
    inline void clear(); // 清零各个成员变量
    void init(xml_node_iterator &it); // 初始化
    inline void update_my_rank();

    // 麻将术语中英对照：http://www.xqbase.com/other/mahjongg_english.htm
    void num_add(int player, int num); // 摸牌
    void num_remove(int player, int num); // 弃牌
    void show_add_from_other(int my, int other, int num); // 拿了别人一张牌副露
    inline void show_add_from_myself(int player, int num); // 自己副露。（暗杠，加杠）

    inline unsigned char sum_4_char(unsigned char *chr); // 把连续的4个char相加。一次加载，二分相加

    void can_chi(int player, int num);
    void can_pon_and_ming_kan(int player, int num);
    void can_an_kan_jia_kan(int player, int num);
    void can_riichi(int player);

    string num_analyse(unsigned char hai_num, bool red_dora = true);
    void discard_analyse(const string &op);
    void hai_analyse(int player, const char *hai); // 解析手牌
    void reach_analyse(xml_node_iterator &it); // 解析立直
    void N_analyse(xml_node_iterator &it); // 解析鸣牌情况
    void dora_analyse(xml_node_iterator &it); // 解析新翻出的dora;

    void analyse_next_op(xml_node_iterator &next_op_it);
    void next_op_discard_analyse(const string &op); // 解析下一个弃牌操作
    void next_op_N_analyse(xml_node_iterator &it); // 解析下一个鸣牌操作
    void next_op_reach_analyse(xml_node_iterator &it); // 解析下一个立直操作

    void update_ostringstream(); // 更新out流，加入当前局面信息

    void hai_to_34(unsigned char short_hai[34]);
    string array_to_str(unsigned char *hai);
    void array_red_dora(unsigned char *hai, string &str); // 解析array中的红5并加入string
    string set_to_str(set<unsigned char> &hai);
    void set_red_dora(set<unsigned char> &hai, string &str); // 解析set中的红5并加入string

    unsigned int BKDRhash(const char *str);

public:
    confrontation(); // 构造函数。初始化四个csv的表头标题
    void input(const char *path, int input_perspective = -1); // 含欲读入xml的路径的构造函数
    void output_csv(string path); // 输出当前out流内容到文件
};

void confrontation::update_point(const char_t *ten)
{
    char buf[32];
    strcpy(buf, ten);
    point[0] = atoi(strtok(buf, ","));
    for(int i = 1; i < 4; i++)
        point[i] = atoi(strtok(NULL, ","));
    
    update_my_rank();
}

void confrontation::clear()
{
    next_op.op = next_op_else;
    next_op.who = 5;
    for(int i = 0; i < player_num; i++)
    {
        memset(hai[i], 0, hai_len);
        memset(discard[i], 0, hai_len);
        discard_reach_state[i][0].clear();
        discard_reach_state[i][1].clear();
        show[i].clear();
        discard_from_hand[i].clear();

        for(int j = 0; j < 3; j++)
            discard_state[i][j].clear();
    }
    dora.clear();
    memset(discard_reach, 0xFF, player_num);
    memset(discard_num, 0, player_num);
    memset(reach, 0, player_num);
    memset(show_num, 0, player_num);
    memset(recently_reach, 0, player_num);
    memset(state, 0, sizeof(state));
}

void confrontation::init(xml_node_iterator &it)
{
    clear();

    auto ait = it->attributes_begin();
    const char_t *seed = (ait++)->value();
    const char_t *ten = (ait++)->value();

    char buf[32];
    strcpy(buf, seed);
    field            = atoi(strtok(buf, ","));
    int banker_times = atoi(strtok(NULL, ","));
    int reach        = atoi(strtok(NULL, ","));
    int dice1        = atoi(strtok(NULL, ","));
    int dice2        = atoi(strtok(NULL, ","));
    int dora_num     = atoi(strtok(NULL, ","));
    
    dora.insert(dora_num);

    update_point(ten);

    auto attr = it->attribute("hai0");
    for(int i = 0; i < player_num; i++)
    {
        hai_analyse(i, attr.value());
        attr = attr.next_attribute();
    }
}

void confrontation::update_my_rank()
{
    int rank = 0;
    for(int i = (perspective + 1) % player_num, temp = point[perspective]; i != perspective; i = (i + 1) % player_num)
        if(temp < point[i] || temp == point[i] && i < perspective)
            rank++;
    my_rank = rank;
}

void confrontation::reach_analyse(xml_node_iterator &it) // 解析立直
{
    int my = it->attribute("who").as_int();
    if(*it->attribute("step").value() == '1') // 宣告立直
    {
        reach[my] = 1;
        recently_reach[my] = 2;

        state[my].can_discard = 1; // 宣告立直了就可以丢牌了，原先忘记考虑这个
    }
    else // step == 2。放下立直棒
        update_point(it->attribute("ten").value());
}

// 麻将术语中英对照：http://www.xqbase.com/other/mahjongg_english.htm
void confrontation::num_add(int player, int num) // 摸牌
{
    hai[player][num] = 1;

    state[player].can_discard = 1; // 摸牌了就可以丢牌了

    if(player == perspective) // 如果是自己就要判断能否立直
    {
        can_an_kan_jia_kan(player, num);
        can_riichi(player);
    }

    if(next_op.op == next_op_discard && next_op.who == player && next_op.hai != num) // 如果下一个操作是自己弃牌，但弃的不是现在摸起的这张，就是手切
        discard_from_hand[player].push_back(next_op.hai);  
}

void confrontation::num_remove(int player, int num) // 弃牌
{
    hai[player][num] = 0;
    discard[player][num] = 1;

    unsigned int state = discard_num[player]++ / 6; // 先算出当前属于早中晚巡的哪一个，再让弃牌数自增
    if(state > 2)
        state = 2;
    discard_state[player][state].insert(num); // 加入对应巡的弃牌

    // 更新立直前舍牌,立直宣言牌,立直后舍牌
    if(recently_reach[player] == 2) // 如果是一发巡，那这张牌是立直宣言牌
    {
        recently_reach[player] = 1; // 转了一圈，回到自己如果不是自摸而是丢牌就没有一发了
        discard_reach[player] = num; // 更新立直宣言牌
    }
    else
    {
        recently_reach[player] = 0;
        discard_reach_state[player][reach[player]].insert(num);
    }

    if(player != perspective) // 不是自己丢的就要考虑碰、明杠
        can_pon_and_ming_kan(perspective, num);

    if(player == (perspective + player_num - 1) % player_num) // 如果是上家弃牌就要判断自己能不能吃
        can_chi(perspective, num);
    
    showed_num = static_cast<unsigned char>(num); // 记录当前可能被副露的牌
}

void confrontation::show_add_from_other(int my, int other, int num) // 拿了别人一张牌副露
{
    show[my].insert(num);
    show_num[my]++;

    state[my].can_discard = 1; // 要准备丢牌
}

inline void confrontation::show_add_from_myself(int player, int num) // 自己副露。（暗杠，加杠）
{
    hai[player][num]--;
    show[player].insert(num);
}

string confrontation::num_analyse(unsigned char hai_num, bool red_dora)
{
    static const char suit_name[] = {'m', 'p', 's', 'z'};
    static const char *num_to_char = &int_to_char[1]; // 牌的实际点数是num + 1

    unsigned int temp = hai_num >> 2;
    unsigned int suit = temp / 9; // 花色
    unsigned int num = temp % 9; // 数字

    // 解析本张牌
    string str;
    str = suit_name[suit];
    str += num_to_char[num];

    if(red_dora)
    {
        // 解析红5
        unsigned int red_dora_num = 0;
        for(int i = 4 * 4, j = 1; i <= 4 * 4 + 2 * 4 * 9; i += 4 * 9, j <<= 1)
            if(i == hai_num)
                red_dora_num |= j;

        str += ',';
        if(red_dora_num)
            str += int_to_char[red_dora_num];
    }

    return str;
}

void confrontation::discard_analyse(const string &op)
{
    int pai = stoi(op.substr(1, op.length() - 1)); // 被操作的那张牌
    char temp = op[0];
    if(temp <= 'G') // 丢弃
        num_remove(temp - 'D', pai);
    else // 摸起
        num_add(temp - 'T', pai);
}

void confrontation::hai_analyse(int player, const char *hai) // 解析手牌
{
    char buf[64], *ptr;
    strcpy(buf, hai);
    
    ptr = strtok(buf, ",");
    while(ptr != NULL)
    {
        num_add(player, atoi(ptr));
        ptr = strtok(NULL, ",");
    }
}

void confrontation::N_analyse(xml_node_iterator &it) // 解析鸣牌情况
{
    memset(recently_reach, 0, sizeof(recently_reach)); // 鸣牌了就没有一发了

    int val = it->attribute("m").as_int();
    int my = it->attribute("who").as_int();
    int other = (my + val) & 0x3;

    if(val & 0x4) // 吃。bit2 == 1
    {
        int base_and_called = val >> 10;
        int base = base_and_called / 3; // 吃中最小的那张牌
        int called = base_and_called % 3; // 被吃的那张牌是第几张
        base += (base / 7) << 1; // 花色、配牌的两倍，用于计算牌编码时的偏移
        int chi[3]; // 吃中三张牌的0~140编码
        for(int i = 0, j = 3; i < 3; i++, j += 2)
            chi[i] = ((base + i) << 2) + ((val >> j) & 0x3);

        show_add_from_other(my, other, chi[called]);
        for(int i = 0; i < 3; i++)
            if(i != called)
                show_add_from_myself(my, chi[i]);        
    }
    else if(val & 0x18) // 碰，bit3 == 1；加杠，bit4 == 1
    {
        int base_and_called = val >> 9;
        int base = base_and_called / 3; // 碰的牌
        int called = 3 - base_and_called % 3; // 被碰的那张牌是第几张。这里要用3来减，网上的资料全错
        base <<= 2; // 乘以4，算出4张相同牌中的最小编号
        int unused = (val >> 5) & 0x3; // 碰：同种的四张牌中在碰中没出现的那张；加杠：加杠的牌。

        if(val & 0x8) // 碰，bit3 == 1
        {
            show_add_from_other(my, other, base + called);
            for(int i = 0; i < 4; i++)
                if(i != unused && i != called)
                    show_add_from_myself(my, base + i);
        }
        else // 加杠
            show_add_from_myself(my, base + unused);
    }
    else // 杠。bit2~bit4都为0
    {
        int base_and_called = val >> 8; // 杠的牌
        int base = base_and_called & 0xFFFFFFFC; // 杠的牌同种的四张牌中最小的那张

        if(my == other) // 暗杠
        {
            for(int i = base; i < base + 4; i++)
                show_add_from_myself(my, i);
        }
        else // 明杠
        {
            show_add_from_other(my, other, base_and_called);
            for(int i = base; i < base + 4; i++)
                if(i != base_and_called)
                    show_add_from_myself(my, i);
        }
    }
}

void confrontation::dora_analyse(xml_node_iterator &it) // 解析新翻出的dora
{
    int dora_num = it->attribute("hai").as_int();
    dora.insert(dora_num);
}

void confrontation::analyse_next_op(xml_node_iterator &next_op_it)
{
    next_op.op = next_op_else;

    const char *name = next_op_it->name();
    if(name[1] >= '0' && name[1] <= '9') // 如果第二位是数字，说明是摸切，则不带参数，直接保存标题
        next_op_discard_analyse(string(name));
    else
    {
        static const char *op_name[] = {"N", "REACH"};
        void (confrontation::*op_fun[])(xml_node_iterator &it) = {&confrontation::next_op_N_analyse, &confrontation::next_op_reach_analyse};
        
        for(unsigned int i = 0; i < sizeof(op_name) / sizeof(char *); i++)
            if(strcmp(name, op_name[i]) == 0)
            {
                (this->*op_fun[i])(next_op_it);
                break;
            }
    }
}

void confrontation::next_op_discard_analyse(const string &op) // 解析下一个弃牌操作
{
    int hai = stoi(op.substr(1, op.length() - 1)); // 被操作的那张牌
    next_op.hai = hai;

    char temp = op[0];
    if(temp <= 'G') // 丢弃
    {
        next_op.op = next_op_discard;
        next_op.who = temp - 'D';
    }
    else // 摸起
    {
        next_op.op = next_op_else;
        next_op.who = temp - 'T';
    }
}

void confrontation::next_op_N_analyse(xml_node_iterator &it) // 解析下一个鸣牌操作
{
    int val = it->attribute("m").as_int();
    int my = it->attribute("who").as_int();

    next_op.who = my;

    if(val & 0x4) // 吃。bit2 == 1
    {
        next_op.op = next_op_chi;

        int base_and_called = val >> 10;
        int base = base_and_called / 3; // 吃中最小的那张牌
        int called = base_and_called % 3; // 被吃的那张牌是第几张
        base += (base / 7) << 1; // 花色、配牌的两倍，用于计算牌编码时的偏移
        next_op.hai = ((base + called) << 2) + ((val >> (3 + 2 * called)) & 0x3); // 被吃的牌0~140的编码
        next_op.hai |= called << 8; // 8~9位用来存操作的是左中右之一
    }
    else if(val & 0x8) // 碰，bit3 == 1
    {
        next_op.op = next_op_pon;  // next_op.hai。吃不需要写出吃的牌 

        int base_and_called = val >> 9;
        int base = base_and_called / 3; // 碰的牌
        int called = 3 - base_and_called % 3; // 被碰的那张牌是第几张。这里要用3来减，网上的资料全错
        base <<= 2; // 乘以4，算出4张相同牌中的最小编号 
        next_op.hai = base + called; // 被碰的牌0~140的编码
    }
    else // 其他操作。如杠、加杠
        next_op.op = next_op_else;
}

void confrontation::next_op_reach_analyse(xml_node_iterator &it) // 解析下一个立直操作
{
    int my = it->attribute("who").as_int();
    next_op.who = my;
    if(*it->attribute("step").value() == '1') // 宣告立直
        next_op.op = next_op_reach;
    else // step == 2。放下立直棒
        next_op.op = next_op_else;
}

string confrontation::array_to_str(unsigned char *hai)
{
    static const char suit_name[] = {'m', 'p', 's'};
    
    string str;

    int num = 0;
    bool suit_non_exist; // 标记该花色是否有牌

    // 解析非字牌
    for(int suit = 0; suit < 3; suit++)
    {
        suit_non_exist = true;
        for(unsigned char i = '1'; i <= '9'; i++)
            for(int j = 0; j < 4; j++, num++)
                if(hai[num])
                {
                    if(suit_non_exist)
                    {
                        str += suit_name[suit];
                        suit_non_exist = false;
                    }
                    str += i;
                }
    }

    // 解析字牌
    suit_non_exist = true;
    for(unsigned char i = '1'; i <= '7'; i++)
        for(int j = 0; j < 4; j++, num++)
            if(hai[num])
            {
                if(suit_non_exist)
                {
                    str += 'z';
                    suit_non_exist = false;
                }
                str += i;
            }
    
    return str;
}

void confrontation::array_red_dora(unsigned char *hai, string &str) // 解析array中的红5并加入string
{
    unsigned char red_dora = 0;
    for(unsigned int i = 4 * 4, j = 1; i <= 4 * 4 + 2 * 4 * 9; i += 4 * 9, j <<= 1)
        if(hai[i])
            red_dora |= j;
    str += ',';
    if(red_dora)
        str += int_to_char[red_dora];
}

string confrontation::set_to_str(set<unsigned char> &hai)
{
    if(hai.empty())
        return string();

    static const char suit_name[] = {'m', 'p', 's', 'z'};
    static const char *num_to_char = &int_to_char[1]; // 牌的实际点数是num + 1

    // 解析牌
    string temp_str[4]; // 四种不同花色的牌
    for(const auto &i : hai)
    {
        unsigned char temp = i >> 2;
        unsigned char suit = temp / 9; // 花色
        unsigned char num = temp % 9; // 数字
        
        temp_str[suit] += num_to_char[num];
    }

    string str;
    for(int i = 0; i < 4; i++) // 将四种不同花色的牌组合到一起，并加上花色标记字符
        if(!temp_str[i].empty())
            str += suit_name[i] + temp_str[i];
    
    //hai.clear();

    return str;
}

void confrontation::set_red_dora(set<unsigned char> &hai, string &str) // 解析set中的红5并加入string
{
    unsigned char red_dora = 0;
    for(unsigned int i = 4 * 4, j = 1; i <= 4 * 4 + 2 * 4 * 9; i += 4 * 9, j <<= 1)
        if(hai.find(i) != hai.end())
            red_dora |= j;
    str += ',';
    if(red_dora)
        str += int_to_char[red_dora];
}

void confrontation::update_ostringstream()
{
    if(static_cast<unsigned int>(state_ptr[perspective]) == 0) // 可以操作才输出
        return;
    
    int i;
    string str;

    // 输出玩家编号
    str = int_to_char[perspective];

    // 输出小局数（0是东1局）
    str += ',';
    str += to_string(field);

    // 输出自家顺位
    str += ',';
    str += int_to_char[my_rank];

    // 输出自家手牌
    str += ',' + array_to_str(hai[perspective]);
    // 输出自家手牌红5
    array_red_dora(hai[perspective], str);

    // 输出自家舍牌,自家舍牌红5
    str += ',' + array_to_str(discard[perspective]);
    // 输出自家舍牌红5
    array_red_dora(discard[perspective], str);

    // 输出下家、对家、上家的早巡舍牌、中巡舍牌、晚巡舍牌、立直前舍牌,立直宣言牌,立直后舍牌,舍牌红5
    for(i = (perspective + 1) % player_num; i != perspective; i = (i + 1) % player_num)
    {
        // 早巡舍牌、中巡舍牌、晚巡舍牌
        for(int j = 0; j < 3; j++)
            str += ',' + set_to_str(discard_state[i][j]);

        // 立直前舍牌
        str += ',' + set_to_str(discard_reach_state[i][0]);

        // 立直宣言牌
        str += ',';
        if(discard_reach[i] != 0xFF)
            str += num_analyse(discard_reach[i], false);

        // 立直后舍牌
        str += ',' + set_to_str(discard_reach_state[i][1]);

        // 舍牌红5
        array_red_dora(discard[i], str);
    }

    // 输出四家副露
    i = perspective;
    do
    {
        str += ',' + set_to_str(show[i]);
        set_red_dora(show[i], str);
        i = (i + 1) % player_num;
    }
    while (i != perspective);
    
    // 输出宝牌指示牌
    str += ',' + set_to_str(dora);

    // 输出下家立直，对家立直，上家立直
    for(i = (perspective + 1) % player_num; i != perspective; i = (i + 1) % player_num)
    {
        str += ',';
        if(reach[i])
            str += int_to_char[reach[i]];
    }

    // 输出下家立直一发,对家立直一发,上家立直一发
    for(i = (perspective + 1) % player_num; i != perspective; i = (i + 1) % player_num)
    {
        str += ',';
        if(recently_reach[i])
            str += int_to_char[recently_reach[i]];
    }

    // 输出X家最近一次手切,X家最近一次手切红5,X家最近二次手切,X家最近二次手切红5,X家最近三次手切,X家最近三次手切红5
    for(i = (perspective + 1) % player_num; i != perspective; i = (i + 1) % player_num)
    {
        auto it = discard_from_hand[i].rbegin();
        if(it != discard_from_hand[i].rend())
            it++; // rbegin()是下一个操作的弃牌，是未来的事情，不应该在这里输出。
        
        for(int j = 0; j < 3; j++) // 最近三次
        {
            str += ',';
            if(it != discard_from_hand[i].rend())
                str += num_analyse(*it++);
            else
                str += ',';
        }
    }

    // temp_state[0]为discard，[1]为chi，[2]为pon，[3]为riichi
    unsigned char *temp_state = reinterpret_cast<unsigned char *>(&state[perspective]);
    
    string temp_str;

    if(state[perspective].can_discard && next_op.op == next_op_discard && next_op.who == perspective) // 操作了且下个操作是弃牌且操作的玩家是自己
    {
        temp_str = str + "," + num_analyse(next_op.hai) + ",\n";
        out_ss[0] << temp_str;
        vec_index[0].push_back(*vec_index[0].rbegin() + temp_str.length()); 
    }

    if(state[perspective].can_chi)
    {
        ostringstream &out = out_ss[1];
        temp_str = str + ",";
        if(next_op.op == next_op_chi && next_op.who == perspective) // 下个操作是弃牌且操作的玩家是自己
            temp_str += to_string((next_op.hai >> 8) + 1) + "," + num_analyse(next_op.hai & 0xFF);
        else
            temp_str += "0," + num_analyse(showed_num);
        
        temp_str += ",\n";
        vec_index[1].push_back(*vec_index[1].rbegin() + temp_str.length());
        out << temp_str;
    }

    if(state[perspective].can_pon)
    {
        ostringstream &out = out_ss[2];
        temp_str = str + ",";
        if(next_op.op == next_op_pon && next_op.who == perspective) // 下个操作是弃牌且操作的玩家是自己
            temp_str += "1," + num_analyse(next_op.hai);
        else
            temp_str += "," + num_analyse(showed_num);
        
        temp_str += ",\n";
        vec_index[2].push_back(*vec_index[2].rbegin() + temp_str.length());
        out << temp_str;
    }

    // 输出立直操作
    if(state[perspective].can_riichi)
    {
        ostringstream &out = out_ss[3];
        temp_str = str + ",";
        if(next_op.op == next_op_reach && next_op.who == perspective) // 下个操作是弃牌且操作的玩家是自己
            temp_str += "1";
        temp_str += ",\n";
        vec_index[3].push_back(*vec_index[3].rbegin() + temp_str.length());
        out << temp_str;
    }
}

confrontation::confrontation()
{
    for(int i = 0; i < player_num; i++)
        discard_from_hand[i].reserve(24); // 预留手切的vector空间

    string col_name("玩家编号,小局数,自家顺位,自家手牌,自家手牌红5,自家舍牌,自家舍牌红5,下家早巡舍牌,下家中巡舍牌,下家晚巡舍牌,下家立直前舍牌,下家立直宣言牌,下家立直后舍牌,下家舍牌红5,对家早巡舍牌,对家中巡舍牌,对家晚巡舍牌,对家立直前舍牌,对家立直宣言牌,对家立直后舍牌,对家舍牌红5,上家早巡舍牌,上家中巡舍牌,上家晚巡舍牌,上家立直前舍牌,上家立直宣言牌,上家立直后舍牌,上家舍牌红5,自家副露,自家副露红5,下家副露,下家副露红5,对家副露,对家副露红5,上家副露,上家副露红5,宝牌指示牌,下家立直,对家立直,上家立直,下家立直一发,对家立直一发,上家立直一发,下家最近一次手切,下家最近一次手切红5,下家最近二次手切,下家最近二次手切红5,下家最近三次手切,下家最近三次手切红5,对家最近一次手切,对家最近一次手切红5,对家最近二次手切,对家最近二次手切红5,对家最近三次手切,对家最近三次手切红5,上家最近一次手切,上家最近一次手切红5,上家最近二次手切,上家最近二次手切红5,上家最近三次手切,上家最近三次手切红5,");
    out_ss[0] << col_name << "操作的牌,操作的牌红5," << endl;
    for(int i = 1; i < 3; i++)
        out_ss[i] << col_name << "是否操作,操作的牌,操作的牌红5," << endl;
    out_ss[3] << col_name << "是否操作," << endl;

    for(int i = 0; i < 4; i++)
        vec_index[i].push_back(out_ss[i].str().length());
}

void confrontation::input(const char *path, int input_perspective)
{
    xml_document xml;

    if(!xml.load_file(path))
    {
        cout << "Load XML: " << path << " failed!" << endl;
        return;
    }

    if(input_perspective == -1)
        perspective = BKDRhash(path) % player_num; // 通过每一局的唯一标识符的hash值确定当前局随机选择的视角
    else
        perspective = input_perspective;

    xml_node mjloggm = xml.child("mjloggm");

    // tag::code[]
    bool is_init = false;
    clear();

    for (xml_node_iterator it = mjloggm.begin(); it != mjloggm.end(); ++it)
    {
        if(reach[perspective]) // 去掉立直后的数据
           is_init = false; // 其实这里init的意思不再是init了，只是利用这个变量让程序不再解析本小局中后续的操作

        auto next_it = it;
        if(++next_it != mjloggm.end())
            analyse_next_op(next_it);

        const char *name = it->name();
        //cout << name << endl;
        if(strcmp(name, "INIT") == 0)
        {
            is_init = true;
            init(it);
        }
        else if(is_init)
        {
            memset(state, 0, sizeof(state)); // 新的操作，初始化标记变量

            if(name[1] >= '0' && name[1] <= '9') // 如果第二位是数字，说明是摸切，则不带参数，直接保存标题
            {
                discard_analyse(string(name));
                update_ostringstream();
            }
            else
            {
                static const char *op_name[] = {"N", "REACH", "DORA"};
                void (confrontation::*op_fun[])(xml_node_iterator &it) = {&confrontation::N_analyse, &confrontation::reach_analyse, &confrontation::dora_analyse};
                
                for(unsigned int i = 0; i < sizeof(op_name) / sizeof(char *); i++)
                    if(strcmp(name, op_name[i]) == 0)
                    {
                        (this->*op_fun[i])(it);
                        if(op_fun[i] != &confrontation::dora_analyse) // 翻出dora无须更新
                            update_ostringstream();
                        break;
                    }
            }
        }
    }
}

void confrontation::can_chi(int player, int num)
{
    num >>= 2;
    int suit = num / 9; // 花色
    num = num % 9; // 数值 - 1

    int start = max(0, num - 2); // 起始数值 - 1
    int end = min(num + 2, 8) - 2; // 终止数值 - 1
    unsigned int *temp_hai = reinterpret_cast<unsigned int *>(&hai[player][suit * 9 * 4]); // 该花色的起始偏移
    if(suit < 3) // 不是字牌
    {
        for(int i = start; i <= end; i++)
        {
            bool if_can_chi = true;
            for(int j = i; j < i + 3; j++)
                if(j != num && temp_hai[j] == 0) // 不是缺的那一张牌，且该张牌不存在，那么就不能吃
                {
                    if_can_chi = false;
                    break;
                }
            
            if(if_can_chi)
            {
                state[player].can_chi = 1;
                break;
            }
        }
    }
}

unsigned char confrontation::sum_4_char(unsigned char *chr) // 一次加载，二分相加
{
    // 将以下循环展开。否则GCC会读分四次读内存然后加到al
    // for(int i = num; i < num + 4; i++)
    //     count += chr[0][i];
    unsigned int &temp1 = *reinterpret_cast<unsigned int *>(chr);
    unsigned short temp2 = static_cast<unsigned short>(temp1) + static_cast<unsigned short>((temp1 >> 16));
    unsigned char count = static_cast<unsigned char>(temp2) + static_cast<unsigned char>((temp2 >> 8));
    return count;
}

void confrontation::can_pon_and_ming_kan(int player, int num)
{
    num &= 0xFFFFFFFC;

    switch(sum_4_char(&hai[player][num]))
    {
        case 2:
            state[player].can_pon = 1;
            break;
        case 3:
            state[player].can_pon = 1;
            state[player].can_kan = 1;
            break;
    } 
}

void confrontation::can_an_kan_jia_kan(int player, int num)
{
    num &= 0xFFFFFFFC;

    if(sum_4_char(&hai[player][num]) == 4 || sum_4_char(&discard[player][num]) == 3) // 能暗杠就不可能加杠了，因而前半部分判断暗杠，后半部分判断加杠
        state[player].can_kan = 1;
}

void confrontation::hai_to_34(unsigned char short_hai[34])
{
    for(int i = 0, j = 0; i < hai_len; i += 4, j++) // 4路循环展开，SSE指令集
    {
        short_hai[j] += hai[0][i];
        short_hai[j] += hai[0][i + 1];
        short_hai[j] += hai[0][i + 2];
        short_hai[j] += hai[0][i + 3];
    }
}

void confrontation::output_csv(string path) // 输出当前out流内容到文件
{
    ofstream fout;
    
    string postfix_csv[4] = {"_discard.csv", "_chi.csv", "_pon.csv", "_riichi.csv"};
    for(int i = 0; i < 4; i++)
    {
        fout.open(path + postfix_csv[i]);
        fout << out_ss[i].str();
        fout.close();
    }

    string postfix_index[4] = {"_discard.index", "_chi.index", "_pon.index", "_riichi.index"};
    for(int i = 0; i < 4; i++)
    {
        vec_index[i].pop_back();
        fout.open(path + postfix_index[i], ios::binary);
        fout.write(reinterpret_cast<const char *>(vec_index[i].data()), vec_index[i].size() * sizeof(size_t));
        fout.close();
    }
}

void confrontation::can_riichi(int player)
{
    unsigned char temp_hai[34] = {0};
    hai_to_34(temp_hai);
    if(show_num[player] == 0 && riichi::calcshanten(temp_hai, 0) == 0)
        state[player].can_riichi = 1;
}

unsigned int confrontation::BKDRhash(const char *str)
{
    unsigned int seed = 131;
    unsigned int hash = 0;
    while(*str)
        hash = hash * seed + (*str++);
    return hash;
}