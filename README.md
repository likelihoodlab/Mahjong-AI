# Mahjong-AI

#### 介绍
一个简单的卷积神经网络构建麻将AI的demo，方法来自(arxiv:1906.02146)

#### 软件架构
C++：处理xml数据以及将处理好的数据形成矩阵作为神经网络训练时的迭代器使用
python：神经网络的训练

#### 安装教程
C++部分使用g++8编译，生成程序:``` g++-8 -O3 -std=c++17 -o main main.cpp -lstdc++fs；```
运行格式：./main 数据文件夹路径 生成的csv文件名前缀 生成的csv组数 单个csv所含南风局数 是否输出文件名（0为false，其余为true）
C++部分迭代器动态链接库使用g++8编译：
```g++ -shared -O3 preprocess.cpp -ldl -mavx -o preprocess.so -fPIC```
python训练部分：直接使用python运行脚本文件，自行设置训练集和测试集
python环境：tensorflow, keras, numpy, pandas
#### 使用说明
由于处理牌谱时，形成的信息是相对原来有损的，故不能形成很好的可视化形式，故没有可视化。提供天凤的xml牌谱文件，即可通过本项目内容，进行神经网络的训练。

